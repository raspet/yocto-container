# Changelog

## Version 1.0
  > Initial Commit:
  > 
  > * Create a image that builds from [poky-container](https://github.com/crops/poky-container) 
  >
  > * Include installation of [repo tool](https://gerrit.googlesource.com/git-repo/) to manage
  > all the various yocto layers easier

  - add repo installation
