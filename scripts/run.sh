#!/bin/bash

IMAGE_VER=1.0
IMAGE=raspet:${IMAGE_VER}

CONTAINER_NAME=yocto_builder
VOL_NAME=sstate_cache

CMD_ARGS=/bin/bash

IS_CREATED=`docker ps -q -f name=${CONTAINER_NAME}`

if [ ! "${IS_CREATED}" ]; then
    docker run -itd --name ${CONTAINER_NAME} -v ${VOL_NAME}:/home/yoctouser/build ${IMAGE} ${CMD_ARGS}
fi

docker exec -it ${CONTAINER_NAME} ${CMD_ARGS}
