#!/bin/bash

. ./version

docker build --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') --build-arg RASPET_VERSION=${VERSION} --network=host -t ${IMG-"raspet:$VERSION"} .
