#raspet dockerfile

FROM crops/yocto:ubuntu-18.04-base

ARG BUILD_DATE

ARG RASPET_VERSION

ENV WORKDIR="/home/yoctouser"

#didn't know what/how to label but seems like OCI image spec
#is a decent guide
#https://github.com/opencontainers/image-spec/blob/master/annotations.md
LABEL org.opencontainers.image.version=$RASPET_VERSION \
      org.opencontainers.image.authors="Sean Letzer" \
      org.opencontainers.image.created=$BUILD_DATE \
      org.opencontainers.image.source="https://gitlab.com/raspet/yocto-container" \
      org.opencontainers.image.description="Container for setting up a yocto build to then build Raspet OS" \
      org.opencontainers.image.title="Raspet Yocto Container"

#NOTE: from yocto:ubuntu-18.04 the workdir is set to 
#/home/yoctouser so lets create a volume with a mount
#point underneath that for ease-of-use.
#its important that a volume be used
#because when yocto rebuilds SSTATE_CACHE it is long
#like... hours long... we want to reduce rebuilding that
#by caching that in a docker volume
VOLUME ${WORKDIR}/build


#inheriting from crops/yocto container the user will be 'yoctouser'
#so change back to root for run commands to work
USER root

#install needed binaries (note most yocto required binaries are built in inherited dockerfile)
RUN apt-get update && \
    apt-get -y install curl vim && \
    apt-get -y install locales && \
    curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && \
    chmod a+rx /usr/local/bin/repo


#create yocto base directory structure
RUN cd ${WORKDIR} && \
    /usr/local/bin/repo init -u https://gitlab.com/raspet/repo-manifest.git -m raspet.xml && \
    repo sync && \
    mkdir ${WORKDIR}/build && chown yoctouser ${WORKDIR}/build && \
    source ./layers/oe-init-build-env 

#bitbake uses python and python needs a locale set
COPY files/locale /etc/default/locale
ENV LC_ALL "en_US.UTF-8"
ENV LANG=en_US.UTF-8


#let the yoctouser user be part of sudo group
#and give it a password (parent Dockerfile didn't set a passwd)
RUN gpasswd --add yoctouser sudo && \
    echo "yoctouser:yoctouser" | chpasswd

#nice to haves
RUN git clone --depth=1 https://github.com/amix/vimrc.git /opt/vim_runtime && \
    sh /opt/vim_runtime/install_awesome_parameterized.sh /opt/vim_runtime --all

#change back
USER yoctouser

